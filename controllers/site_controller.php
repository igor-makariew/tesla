<?php
    require_once ('core/controller.php');
    //require_once ('models/user.php');

    class Site extends Controller{
            public $core = '';

        public function __construct($core){
            parent::__construct($core);
            $this->core = $core;

            $this->add_css('web/css/bootstrap.min.css');
            $this->add_css('web/css/animate.css');
            $this->add_css('web/css/owl.carousel.min.css');
            $this->add_css('web/css/owl.theme.default.min.css');
            $this->add_css('web/css/magnific-popup.css');
            $this->add_css('web/css/aos.css');
            $this->add_css('web/css/ionicons.min.css');
            //$this->add_css('web/css/flaticon.css');
            //$this->add_css('web/css/icomoon.css');
            $this->add_css('web/css/style.css');

            $this->add_js('web/js/jquery.min.js');
            $this->add_js('web/js/jquery-migrate-3.0.1.min.js');
            $this->add_js('web/js/popper.min.js');
            $this->add_js('web/js/bootstrap.min.js');
            $this->add_js('web/js/jquery.easing.1.3.js');
            $this->add_js('web/js/jquery.waypoints.min.js');
            $this->add_js('web/js/jquery.stellar.min.js');
            $this->add_js('web/js/owl.carousel.min.js');
            $this->add_js('web/js/jquery.magnific-popup.min.js');
            $this->add_js('web/js/aos.js');
            $this->add_js('web/js/jquery.animateNumber.min.js');
            $this->add_js('web/js/scrollax.min.js');
            $this->add_js('web/js/main.js');
            $this->add_js('web/js/script.js');
            $this->add_js('web/js/vue.js');

            $this->add_fonts('https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900');
            $this->set_layout('layout/clark/layout.php');
        }

        function actionIndex(){
            $this->params['title'] = 'Главная';

            // roles
            $this->core->get_core()['user_model']->turn_number_into_string();

            $this->params['get_array_user'] = isset($this->core->get_core()['user_model']->get_array_user()['id']);

            //start send email
            $to = 'igor.malariew@yandex.ru';
            $name = filter_input(INPUT_POST, 'name');
            $email = filter_input(INPUT_POST, 'email');
            $subject = filter_input(INPUT_POST, 'subject');
            $message = filter_input(INPUT_POST, 'message');

            $headers = "Content-type: text/html; charset=windows-1251 \r\n";
            $headers .= "From: От кого письмо {$email}\r\n";
            $headers .= "Reply-To: {$to}\r\n";

            if(!empty($name) && !empty($email) && !empty($subject) && !empty($message)){
                mail($to, $subject, $message, $headers);
            }
            //end send email

            return $this->render('views/site/index.php');
        }

        function actionLogin(){
            $this->params['title'] = 'Login';

            //получение данных от пользователя
            $login = trim(filter_input(INPUT_POST, 'login'));
            $password = trim(filter_input(INPUT_POST, 'password'));
            $submit = filter_input(INPUT_POST, 'submit');

            if(!empty($submit)){
                if(!empty($login) && !empty($password)){
                    if($this->core->get_core()['user_model']->login_user($login, $password)){
                        header('Location: /');
                    }
                }else{
                    $this->params['error'] = false;
                }
            }


            return $this->render('views/site/login.php');
        }

        function actionRegistration(){
            $this->params['title'] = 'Registration';

            //получение данных от пользователя
            $author = trim(filter_input(INPUT_POST, 'author'));
            $login = trim(filter_input(INPUT_POST, 'login'));
            $email =    trim(filter_input(INPUT_POST, 'email'));
            $password = trim(filter_input(INPUT_POST, 'password'));
            $submit = filter_input(INPUT_POST, 'submit');

            if(!empty($submit)){
                if(!empty($author) && !empty($login) && !empty($email) && !empty($password)){
                    $this->core->get_core()['user_model']->add_user($author, $login, $email, $password, USER);
                    header('Location: ?action=site/login');
                }else{
                    $this->params['error'] = false;
                }
            }

            return $this->render('views/site/registration.php');
        }

        function actionLogout(){
            if($this->core->get_core()['user_model']->logout_user($this->core->get_core()['user_model']->get_array_user())){
                header('Location: /');
            }
        }

        function actionProject(){
            $this->params['title'] = 'Projects';

            //получение данных от пользователя
            $author = filter_input(INPUT_POST, 'author');
            $email = filter_input(INPUT_POST, 'email');
            $project = filter_input(INPUT_POST, 'project');
            $code = filter_input(INPUT_POST, 'code');
            //$submit = filter_input(INPUT_POST, 'add_project');

            // значения по умолчанию
            $this->params['error_sql'] = 'false';
            $this->params['error'] = false;
            //var_dump($_POST['add_project']);
            if(isset($_POST['add_project'])) {
                //var_dump($_POST['add_project']);
                if (!empty($author) && !empty($email) && !empty($project) && !empty($code)) {
                    if ($this->core->get_core()['project_model']->set_projects($author, $email, $project, $code)) {
                        $this->params['error_sql'] = 'false';
                        header('Location: ?action=site/project');

                    } else {
                        $this->params['error_sql'] = 'true';
                    }
                } else {
                    if(isset($_POST['add_project'])){
                        $this->params['error'] = true;
                        unset($_POST);
                        header('Location: ?action=site/project');
                    }
                }
            }

            //start sort
            $this->core->get_core()['project_model']->sort_column_project($_GET['column'], $_GET['sort']);
            // end sort

            //start pagination
            if(!isset($_GET['page'])){
                $_GET['page'] = 1;
            }
            $_SESSION['page'] =  $_GET['page'];

            $this->core->get_core()['pagination_model']->set_page($_GET['page']);
            $this->core->get_core()['pagination_model']->set_on_page(10);
            $this->core->get_core()['pagination_model']->set_shift(
                                                                    $this->core->get_core()['pagination_model']->get_array_page($_SESSION['sort'], $_SESSION['column']),
                                                                    $this->core->get_core()['pagination_model']->get_page(),
                                                                    $this->core->get_core()['pagination_model']->get_on_page());
            $this->core->get_core()['pagination_model']->set_count($this->core->get_core()['pagination_model']->count_projects());
            $this->core->get_core()['pagination_model']->set_page_count(ceil(
                                                                    $this->core->get_core()['pagination_model']->count_projects()['count(*)'] /
                                                                    $this->core->get_core()['pagination_model']->get_on_page()));
            $this->params['pages'] = $this->core->get_core()['pagination_model']->get_page_count();
            //выборка из базы данных
            $this->params['projects'] = $this->core->get_core()['pagination_model']->get_shift();
            // end pagination

            // подсчет строк в массиве
            $_SESSION['count_str'] = $this->core->get_core()['pagination_model']->count_id_array($_GET['page']);

            return $this->render('views/site/project.php');
        }

        function actionSearch(){
            $arr = $this->params['projects'] = $this->core->get_core()['project_model']->get_projects();
            foreach ($arr as $key => $item) {
                $array[$key] = substr($item['project'], 0, 30);
            }
            echo json_encode($array);
        }

        function actionSearchall(){
            $arr = $this->params['projects'] = $this->core->get_core()['project_model']->get_projects();

            echo json_encode($arr);
        }

        function actionSearchproject(){
            if(isset($_GET['project'])){
               $arr = $this->core->get_core()['project_model']->get_search_project($_GET['project']);
            }else{
                $arr = false;
            }
            echo json_encode($arr);

        }

        function actionPageproject(){
            $array_page = $this->core->get_core()['project_model']->get_page_project((int)$_GET['page']);

            $this->params['title'] = $array_page['project'];
            $this->params['array_page'] = $array_page;


            return $this->render('views/site/pageproject.php');
        }

        function actionSortauthor(){
            echo $_GET['sort'];
        }

        function actionBlog(){
            $this->params['title'] = 'Blog';

            return $this->render('views/site/blog.php');
        }

        function actionBlogform(){
            // принимаем параметры от axios
            $_POST = json_decode(file_get_contents('php://input'), true);

            $author = $_POST['author'];
            $matter = $_POST['matter'];
            $subject = $_POST['subject'];

            $error_true = true;
            $error_false = false;

            if(!empty($author) && !empty($matter) && !empty($subject)){
                if($this->core->get_core()['blog_model']->set_blog($author, $matter, $subject)){
                    echo json_encode($error_false);
                }else{
                    echo json_encode($error_true);
                }
            }else{
                echo json_encode($error_true);
            }

        }

        function actionBloglists(){
            echo json_encode($this->core->get_core()['blog_model']->get_blog());
        }

        function actionBlogsort(){
            $_POST= json_decode(file_get_contents('php://input'), true);
            $sort = $_POST['sort'];
            $column = $_POST['column'];

            echo json_encode($this->core->get_core()['blog_model']->blog_sort($sort, $column));
        }
    }
