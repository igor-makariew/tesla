<?php
    require_once ('core/core.php');

    ini_set('display_errors', 'on');
    error_reporting(E_ALL);

    // запуск фреймворка
    (new Core())->start();
