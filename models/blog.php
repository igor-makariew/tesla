<?php
    require_once ('core/model.php');

    class Blog extends Model{

        function set_blog($author, $matter, $subject){
            $query = sprintf("INSERT INTO `blog` (`author`, `matter`, `subject`, `date`) VALUES ('%s', '%s', '%s', '%s')",
                            mysqli_real_escape_string($this->get_link(), $author), mysqli_real_escape_string($this->get_link(), $matter),
                            mysqli_real_escape_string($this->get_link(), $subject), mysqli_real_escape_string($this->get_link(), date("Y-m-d H:i:s")));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function get_blog(){
            $query = "SELECT * FROM `blog`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function blog_sort($sort, $column){
            $query = "SELECT * FROM `blog` ORDER BY {$column} {$sort}";
//            $query = sprintf("SELECT * FROM `blog` ORDER BY '%s' '%s'",
//                                        mysqli_real_escape_string($this->get_link(), $column), mysqli_real_escape_string($this->get_link(), $sort));
            $result = mysqli_query($this->get_link(),$query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }
    }
