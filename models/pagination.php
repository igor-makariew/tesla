<?php
require_once ('core/model.php');

class Pagination extends Model{

    public $page;
    public $on_page;
    public $shift;
    public $count;
    public $page_count;
    public $sort;

    function set_page($page){
        $this->page = $page;
    }

    function get_page(){
        return $this->page;
    }

    function set_on_page($on_page){
        $this->on_page =  $on_page;
    }

    function get_on_page(){
        return $this->on_page;
    }

    function set_shift($array, $offset, $length){
        $this->shift = array_slice($array, $length*($offset-1), $length*$offset);
    }

    function get_shift(){
        return $this->shift;
    }

    function set_count($array){
        $this->count = $array;
    }

    function get_count(){
        return $this->count;
    }

    function set_page_count($page_count){
        $this->page_count = $page_count;
    }

    function get_page_count(){
        return $this->page_count;
    }

    function get_array_page($sort, $column){
        $query = "SELECT * FROM `projects` ORDER BY {$column} {$sort}";
        $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
        $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $array_result;
    }

    function count_projects(){
        $query = "SELECT count(*) FROM `projects`";
        $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
        $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
        return $array_result;
    }

    function count_id_array($page){
        $number_page = ($page - 1) * 10;
        return $number_page;
    }
}
