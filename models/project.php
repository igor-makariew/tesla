<?php
    require_once ('core/model.php');

    class Project extends Model{

        function get_projects(){
            $query = "SELECT * FROM `projects`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            if(!empty($array_result)){
                return $array_result;
            }else{
                return false;
            }
        }

        function get_search_project($project){
            $query = sprintf("SELECT * FROM `projects` WHERE `project` = '%s'",
                                    mysqli_real_escape_string($this->get_link(), $project));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $array_result;
        }

        function set_projects($author, $email, $project, $code){
            $query = sprintf("INSERT INTO `projects` (`author`,`email`,`project`,`code`,`date`) VALUES ('%s', '%s', '%s', '%s', '%s')",
                                    mysqli_real_escape_string($this->get_link(), $author), mysqli_real_escape_string($this->get_link(), $email),
                                    mysqli_real_escape_string($this->get_link(), $project), mysqli_real_escape_string($this->get_link(), $code),
                                    date("Y-m-d H:i:s"));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else {
                return false;
            }
        }

        function get_page_project($page){
            $query = sprintf("SELECT * FROM `projects` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $page));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if(mysqli_num_rows($result) == 1){
                $array_result = mysqli_fetch_array($result, MYSQLI_ASSOC);
                return $array_result;
            }else{
                return false;
            }
        }

        function sort_column_project($column, $sort){
            $_SESSION['column'] = $column;
            $_SESSION['sort'] = $sort;
        }
    }

