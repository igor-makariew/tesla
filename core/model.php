<?php
    require_once ('constant.php');

    class Model {
        public $user = 'admin';
        public $localhost = 'localhost';
        public $password = 'admin';
        public $database = 'tesla';
        public $link;

        public function __construct(){
            $this->link = mysqli_connect($this->localhost, $this->user, $this->password, $this->database);
        }

        function get_link(){
            return $this->link;
        }
    }
