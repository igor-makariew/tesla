<?php

    class Url{
        public $path_url;
        public $name_controller = '';
        public $name_action = '';
        public $error;

        public function __construct($path_url){
            $this->path_url = explode('/', $path_url);
            if(count($this->path_url) == 1){
                $this->name_controller = $this->path_url[0];
            }

            if(count($this->path_url) == 2){
                $this->name_controller = $this->path_url[0];
                $this->name_action = $this->path_url[1];
            }

            if(count($this->path_url) > 2){
                $this->error =  false;
            }
        }

        // получение имени controllr
        function get_name_controller(){
            return $this->name_controller;
        }
        // получение имени action
        function get_name_action(){
            return $this->name_action;
        }
    }
