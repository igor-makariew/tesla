<?php
    require_once('core/controller.php');

    class Admin extends Controller{

        public $core;

        public function __construct($core){
            parent::__construct($core);

            $this->core = $core;

            // подключение стилей и скриптов
            $this->add_css('modules/admin/web/css/bootstrap.css');
            $this->add_css('modules/admin/web/css/bootstrap-theme.css');
            $this->add_css('modules/admin/web/css/starter-template.css');
            $this->add_css('modules/admin/web/css/style.css');
//            $this->add_js('modules/admin/web/js/bootstrap.min.js');
            $this->add_js('modules/admin/web/js/jquery.min.js');
//            $this->add_js('https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
//            $this->add_js('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js');
            //$this->add_js('modules/admin/web/js/npm.js');

            //задаем путь шаблона
            $this->set_layout('modules/admin/layout/admin_layout/admin_layout.php');
        }

        function actionIndex(){
            // start проверка роли пользователя
            if($this->core->get_core()['user_model']->array_user['author'] == 1) {
                $this->params['title'] = "Admin index - {$this->core->get_core()['user_model']->array_user['author']}";

                $this->params['avatar'] = isset($this->core->get_core()['user_model']->get_array_user()['author'])?$this->core->get_core()['user_model']->get_array_user()['author']:'no avatar';
                $this->params['get_array_user'] = $this->core->get_core()['user_model']->get_array_user();

            }
            // end проверка роли пользователя

            $this->params['title'] = "Admin index - {$this->core->get_core()['user_model']->array_user['author']}";

            $this->params['avatar'] = isset($this->core->get_core()['user_model']->get_array_user()['author'])?$this->core->get_core()['user_model']->get_array_user()['author']:'no avatar';
            $this->params['get_array_user'] = $this->core->get_core()['user_model']->get_array_user();

            return $this->render('views/admin/index.php');

        }

        function actionDeleteavatar(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $delete_avatar = $_POST['image'];

            //modules/admin/web/images/avatars/1_igor makarev/8k.jpg
            //'C:\OSPanel\domains\tesla\modules\admin\web\images\girls\500.jpg'

            if($this->core->get_core()['images_model']->delete_update_params_avatar($this->core->get_core()['user_model']->get_array_user()['id'])){
                if(unlink($delete_avatar)){
                    echo json_encode(false);
                }else{
                    echo json_encode(true);
                }
            }else{
                echo json_encode(true);
            }

        }

        function actionUpdatepersonaldate(){
            $_POST = json_decode(file_get_contents('php://input'), true);

            $name = $_POST['name'];
            $old_year = $_POST['old_year'];
            $city = $_POST['city'];
            $language = $_POST['language'];
            $image = $_POST['image'];

            $array_true_false = [];
            if(!empty($name) || !empty($old_year) || !empty($city) || !empty($language || !empty($image))){

                foreach($_POST as $key => $value){
                    if($key == 'name' && $value != ''){
                        if($this->core->get_core()['personal_date_model']->update_name_author($this->core->get_core()['user_model']->get_array_user()['id'], $value)){
                            $array_true_false[$key] = 'true';
                        }
                    }else if($value != '' && $key != 'name'){
                        if($this->core->get_core()['personal_date_model']->update_value($this->core->get_core()['user_model']->get_array_user()['id'], $key, $value)){
                            $array_true_false[$key] = 'true';
                        }
                    }else{
                        $array_true_false[$key] = 'false';
                    }
                }
                if($this->core->get_core()['personal_date_model']->return_true_false($array_true_false)){
                    echo json_encode(false);
                }else{
                    echo json_encode(true);
                }
            }
            //echo json_encode($_POST);
        }

        function actionGetpersonaldate(){
            $_POST = json_encode(file_get_contents('php://input'), true);

            $array_number = ['0','1','2','3','4','5','6','7','8','9'];
            $array = explode(':', $_POST);
            // длина слова
            $len = mb_strlen($array[1]);
            $argement = [];
            // выбираем из массива числовое значение
            for($i = 0; $i < $len; $i++){
                foreach ($array_number as $key => $value) {
                    if($value == mb_substr($array[1], $i, 1)){
                        $argement[$key] = $value;
                    }
                }
            }
            $stroka = implode($argement);
            $chislo = (int)$stroka;
//
            echo json_encode($this->core->get_core()['personal_date_model']->get_user($chislo));
        }

        function actionCreateimage(){
            //путь к файлу
            $dir = 'modules/admin/web/images/avatars/'.$this->core->get_core()['user_model']->get_array_user()['id'].'_'.$this->core->get_core()['user_model']->get_array_user()['login'];

            if(!file_exists($dir)){
                mkdir($dir);
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    move_uploaded_file($_FILES['file']['tmp_name'], $dir.'/'.$_FILES['file']['name']);
                    if($this->core->get_core()['images_model']->set_avatar(
                        $this->core->get_core()['user_model']->get_array_user()['id'],
                        $_FILES['file']['name'],
                        $dir.'/'.$_FILES['file']['name']
                    )){
                        echo json_encode(1);
                    }
                }else{
                    echo json_encode(true);
                }
            }else{
                if(isset($_FILES['file']['tmp_name'])){
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){//var_dump($_FILES);
                        move_uploaded_file($_FILES['file']['tmp_name'], $dir.'/'.$_FILES['file']['name']);

                        $param_avatar_old = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);

                        if($param_avatar_old['avatar'] ==  1){  //var_dump($param_avatar_old);

                            if($this->core->get_core()['images_model']->delete_update_params_avatar($this->core->get_core()['user_model']->get_array_user()['id'])){
                                if($this->core->get_core()['images_model']->set_avatar(
                                    $this->core->get_core()['user_model']->get_array_user()['id'],
                                    $_FILES['file']['name'],
                                    $dir.'/'.$_FILES['file']['name']
                                    )
                                ){
                                    $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                                    unlink($param_avatar_old['path_image']);
                                    echo json_encode($param_avatar_new);
                                }
                            }else{
                                echo json_encode(true);
                            }
                        }else{
                            if($this->core->get_core()['images_model']->set_avatar(
                                $this->core->get_core()['user_model']->get_array_user()['id'],
                                $_FILES['file']['name'],
                                $dir.'/'.$_FILES['file']['name']
                            )){
                                $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                                echo json_encode($param_avatar_new);
                            }
                        }
                    }
                }else{
                    $param_avatar_new = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);
                    echo json_encode($param_avatar_new);
                }
            }
        }

        function actionGetavatar(){
            $param_avatar = $this->core->get_core()['images_model']->get_params_avatar($this->core->get_core()['user_model']->get_array_user()['id']);

            echo json_encode($param_avatar);
        }
    }
