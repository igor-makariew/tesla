<?php
    require_once ('core/model.php');

    class Personaldate extends Model{
        //?
        function set_personal_date($array){
            $query = sprintf("INSERT INTO `personal_date` (`name`, `old_year`, `city`, `language`) VALUES ('%s', '%s', '%s', '%s')",
                                        mysqli_real_escape_string($this->get_link(), $array['name']), mysqli_real_escape_string($this->get_link(), $array['old_year']),
                                        mysqli_real_escape_string($this->get_link(), $array['city']), mysqli_real_escape_string($this->get_link(), $array['language'])
            );
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function get_personal_data($id){
            $query = sprintf("SELECT * FROM `personal_date` WHERE `id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            return $array_result;
        }

        function get_user($id){
            $query = sprintf("SELECT users.author, personal_date.old_year, personal_date.city, personal_date.language FROM `users` LEFT JOIN `personal_date` 
                                        ON users.id = personal_date.id_users WHERE users.id = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            return $array_result;
        }

        function update_name_author($id, $name){
            $query = sprintf("UPDATE `users` SET `author` = '%s' WHERE `id` = '%s'",
                                    mysqli_real_escape_string($this->get_link(), $name), mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function update_value($id, $key, $value){
            $query = sprintf("UPDATE `personal_date` SET {$key} = '%s' WHERE `id` = '%s'",
                mysqli_real_escape_string($this->get_link(), $value), mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function return_true_false($array){
            $arr = [];
            foreach ($array as $key => $value){
                if($value == 'true'){
                    $arr[$key] = $value;
                }
            }
            if(count($arr) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
