<?php
    require_once ('core/model.php');

    class Images extends Model{

        function set_avatar($user_id, $name_image, $path_image){
            $query = sprintf("UPDATE `users` SET `avatar` = '%s' WHERE `id` = '%s'",
                                    mysqli_real_escape_string($this->get_link(), 1), mysqli_real_escape_string($this->get_link(), $user_id));
            $result_user = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));

            $query_avatar = sprintf("INSERT INTO `avatar` (`user_id`,`name_image`, `path_image`) VALUES ('%s', '%s', '%s')",
                mysqli_real_escape_string($this->get_link(), $user_id), mysqli_real_escape_string($this->get_link(), $name_image), mysqli_real_escape_string($this->get_link(), $path_image));
            $result_avatar = mysqli_query($this->get_link(), $query_avatar) or die(mysqli_error($this->get_link()));

            if($result_user && $result_avatar){
                return true;
            }else{
                return false;
            }
        }

        function get_params_avatar($id){
            $query = sprintf("SELECT users.avatar, avatar.name_image, avatar.path_image FROM `users` LEFT JOIN `avatar` ON users.id = avatar.user_id  WHERE users.id = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $array_result = mysqli_fetch_assoc($result);
            return $array_result;
        }

        function delete_update_params_avatar($id){
            $query = sprintf("DELETE FROM `avatar` WHERE `user_id` = '%s'", mysqli_real_escape_string($this->get_link(), $id));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));

            $query = sprintf("UPDATE `users` SET `avatar` = '%s' WHERE `id` = '%s'",
                mysqli_real_escape_string($this->get_link(), 0), mysqli_real_escape_string($this->get_link(), $id));
            $result_user = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));

            if($result && $result_user){
                return true;
            }else{
                return false;
            }
        }

        function delete_image($path){
            $query = sprintf("DELETE FROM `avatar` WHERE `path_image` = '%s'", mysqli_real_escape_string($this->get_link(), $path));
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));

            if($result){
                return true;
            }else{
                return false;
            }
        }


    }
